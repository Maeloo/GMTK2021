using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LightLevelSettings", menuName = "LightLevelSettings", order = 1)]
public class LightLevelSettings : ScriptableObject
{
    [Serializable]
    public struct LightLevel
    {
        public float Level;
        public float Radius;
        public float LightIntensity;
        public Color Color;
    };

    public float InterpolationSpeed;
    public List<LightLevel> Levels;

    public LightLevel GetLightLevel(float Level)
    {
        //Debug.Log("GetLightLevel " + Level);
        
        int IndexDown = Mathf.Max(Mathf.FloorToInt(Level), 0);
        LightLevel LightLevelDown = Levels[IndexDown];
        int IndexUp = Mathf.Min(Mathf.CeilToInt(Level), Levels.Count-1);
        LightLevel LightLevelUp = Levels[IndexUp];

        if (IndexDown == IndexUp)
        {
            return LightLevelDown;
        }
        else
        {
            float alpha = Level - LightLevelDown.Level;
            
            //Debug.Log("D " + LightLevelDown.Level + " | U " + LightLevelUp.Level + " | A " + alpha);

            // blend together
            LightLevel result;
            result.Level = Level;
            result.Radius = Mathf.Lerp(LightLevelDown.Radius, LightLevelUp.Radius, alpha);
            result.LightIntensity = Mathf.Lerp(LightLevelDown.LightIntensity, LightLevelUp.LightIntensity, alpha);
            Color mixedColor = new Color(
                Mathf.Lerp(LightLevelDown.Color.r, LightLevelUp.Color.r, alpha),
                Mathf.Lerp(LightLevelDown.Color.g, LightLevelUp.Color.g, alpha),
                Mathf.Lerp(LightLevelDown.Color.b, LightLevelUp.Color.b, alpha),
                Mathf.Lerp(LightLevelDown.Color.a, LightLevelUp.Color.a, alpha));
            result.Color = mixedColor;

            return result;
        }
        
    }
}
