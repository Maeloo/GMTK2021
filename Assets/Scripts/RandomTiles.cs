using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomTiles : MonoBehaviour
{
    public Mesh[] Meshes;
    // Start is called before the first frame update
    void Start()
    {
        int RandomNumberOrient = Random.Range(0, 4);
        transform.localRotation = Quaternion.Euler( 0, 90f * RandomNumberOrient, 0);
        if (Meshes.Length != 0)
        {
            int RandomNumberMeshes = Random.Range(0, Meshes.Length);
            GetComponent<MeshFilter>().mesh = Meshes[RandomNumberMeshes];
            
        }
    }

}
