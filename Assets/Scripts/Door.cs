using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : Interactable
{
    private AudioSource _audio;

    public AudioClip _turnOnSound;
    public AudioClip _turnOffSound;

    void Awake()
    {
        _audio = GetComponent<AudioSource>();
    }

    protected override void OnActivation()
    {
        GetComponent<Animator>().SetBool("isOpen", true);
        _audio.clip = _turnOnSound;
        _audio.Play();

    }
    
    protected override void OnDeactivation()
    {
        GetComponent<Animator>().SetBool("isOpen", false);
        _audio.clip = _turnOffSound;
        _audio.Play();
    }

   
}
