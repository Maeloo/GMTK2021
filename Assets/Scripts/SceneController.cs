using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
    public string _currentScene;
    public string _nextScene;
    public string _mainMenu;

    // Start is called before the first frame update
    void Start()
    {
        _currentScene = UnityEngine.SceneManagement.SceneManager.GetActiveScene().name;
    }
    
    public void Restart()
    {
        MoveToScene(_currentScene);
    }
    
    public void ToNextLevel()
    {
        MoveToScene(_nextScene);
    }

    public void ToMainMenu()
    {
        MoveToScene(_mainMenu);
    }

    private void MoveToScene(string Name)
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(Name);
    }
}
