using System;
using System.Collections;
using System.Collections.Generic;
using MiscUtil.Extensions.TimeRelated;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class Unit : MonoBehaviour
{
    //[SerializeField] private Material _defaultMat;
    //[SerializeField] private Material _selectedMat;
    [SerializeField] private GameObject _selectionObject;
    [SerializeField] private Image _debugImage;
    [SerializeField] private float _maxSpeed = 0.025f;
    [SerializeField] private float _maxAcceleration = 0.05f;
    [SerializeField] private float _rotationRate = 360f;
    [SerializeField] private float _stopDistance = 1.5f;
    [SerializeField] private float _floorCheckDistance = 3f;


    public bool _startLit = true;

    private Color _color;
    private Vector3 _velocity;
    private Vector3 _destination;
    private MovingPlatform _platform;
    private FirePit _firePit;
    private SpriteRenderer _sprite;
    public Animator _animator;
    private Vector3 _lightPos;
    private PlayerController _playerController;
    private AudioSource _audio;
    private Rigidbody _rigidbody;

    private MeshRenderer _meshRenderer;

    public LightEmitterController _light;
    public LightSensor _lightSensor;
    public LightLinker _lightLinker;


    public AudioClip _turnOnSound;
    public AudioClip _turnOffSound;
    public AudioClip _runSound;

    private void Start()
    {
        _audio = GetComponent<AudioSource>();
        Reset();
    }

    public void Reset()
    {
        _playerController = null;
        _meshRenderer = GetComponent<MeshRenderer>();
        _rigidbody = GetComponent<Rigidbody>();
        _destination = transform.position;
        _sprite = GetComponentInChildren<SpriteRenderer>();
        _animator = GetComponent<Animator>();
        _color = Color.black;
        _lightPos = _light.transform.localPosition;
        _selectionObject.SetActive(false);

        if (_startLit)
        {
            LightUp();
        }
        else
        {
            Extinguish();
        }

        Deselect();
    }

    public void Select(PlayerController pc)
    {
        if (_color.Equals(Color.black))
        {
            _color = pc.PopColor();
            _color.a = 1;
        }

        if (_playerController == null)
        {
            _playerController = pc;
        }
        
        //_meshRenderer.material = _selectedMat;
        _selectionObject.SetActive(true);
    }

    public void Deselect()
    {
        //_meshRenderer.material = _defaultMat;
        _selectionObject.SetActive(false);
    }

    public bool IsSelected()
    {
        return _selectionObject.gameObject.activeSelf;
    }

    public void MoveTo(Vector3 location)
    {
        _destination = location;
        if ((location - transform.position).x > 0)
        {
            _sprite.flipX = true;
            _light.transform.localPosition = new Vector3(-_lightPos.x, _lightPos.y, _lightPos.z);
        }
        else
        {
            _sprite.flipX = false;
            _light.transform.localPosition = _lightPos;
        }
    }

    public void AddMoveOffset(Vector3 offset)
    {
        _destination += offset;
    }

    private void Update()
    {
        // Movement
        if (!IsInLight())
        { 
            _velocity = Vector3.zero;
            _rigidbody.velocity = Vector3.zero;
            _destination = transform.position;
            
            _animator.SetBool("isInLight", false);
            _animator.SetBool("isMoving", false);
        }
        else if (Vector3.Distance(_destination, transform.position) > _stopDistance)
        {
            var direction = Vector3.ProjectOnPlane(_destination - transform.position, Vector3.up).normalized;

            // Apply rotation
            var forward = Vector3.ProjectOnPlane(transform.forward, Vector3.up).normalized;
            var offsetRotation = Vector3.Angle(forward, direction);
            var deltaRotation = Mathf.Min(_rotationRate * Time.deltaTime, offsetRotation);
            var updateRotation = deltaRotation * Mathf.Sign(Vector3.Dot(forward, direction));

            //transform.Rotate(Vector3.up, updateRotation);

            _velocity = Vector3.RotateTowards(_velocity, direction, Mathf.Deg2Rad * _rotationRate * Time.deltaTime, 0f);

            // Apply acceleration
            var acceleration = direction * _maxAcceleration * Time.deltaTime;

            _velocity += acceleration;
            if (_velocity.magnitude > _maxSpeed)
            {
                var scale = _maxSpeed / _velocity.magnitude;
                _velocity = new Vector3(_velocity.x * scale, _velocity.y * scale, _velocity.z * scale);
            }
            
            _rigidbody.velocity = _velocity * 250f;
            
            _animator.SetBool("isInLight", true);
            _animator.SetBool("isMoving", true);
            
            _audio.clip = _runSound;
            _audio.loop = true;
           
            if (!_audio.isPlaying) _audio.Play();
        }
        else
        {
            _velocity = Vector3.zero;
            _rigidbody.velocity = Vector3.zero;
            _destination = transform.position;
            
            _animator.SetBool("isInLight", true);
            _animator.SetBool("isMoving", false);
            _audio.clip = _runSound;
            if (_audio.isPlaying && _audio.clip == _runSound) _audio.Stop();
        }

        // Floor check
        var ray = new Ray
        {
            origin = transform.position,
            direction = -Vector3.up
        };

        if (Physics.Raycast(ray, out var hit, _floorCheckDistance))
        {
            var platform = hit.collider.GetComponent<MovingPlatform>();
            if (platform != _platform)
            {
                if (platform != null)
                {
                    _platform = platform;
                    _platform.Register(this);
                }
                else
                {
                    _platform.Unregister(this);
                    _platform = null;
                }
            }
        }
    }

    private float _elapsedStuckTime = 0f;
    public void OnCollisionStay(Collision collision)
    {
        if (_rigidbody.velocity.sqrMagnitude > 0f && _rigidbody.velocity.sqrMagnitude < 0.01f)
        {
            _elapsedStuckTime += Time.deltaTime;
            if (_elapsedStuckTime > 1f)
            {
                _rigidbody.velocity = Vector3.zero;
                _destination = transform.position;    
            }
        }
        else
        {
            _elapsedStuckTime = 0f;
        }
    }

    public void NotifyNearFirePit(FirePit firePit)
    {
        _firePit = firePit;
    }
    public void NotifyLeaveFirePit()
    {
        _firePit = null;
    }

    public bool HasNearFirePit(out FirePit firePit)
    {
        firePit = _firePit;
        return firePit != null;
    }

    public bool IsInLight() 
    {
        return _lightSensor.IsLit() || _light.IsLit(); 
    }

    public bool CanTorchUpLight()
    {
        var bNearValidFirePit = _firePit != null && _firePit._light.IsLit();
        var bNearValidTorch = _playerController != null && _playerController.IsUnitNearOtherLitUnit(this);
        return bNearValidTorch || bNearValidFirePit;
    }

    public void OnTorchOnInput()
    {
        if (!_light.IsLit() && CanTorchUpLight())
        {
            LightUp();
        }
    }

    public void OnTorchOffInput()
    {
        if (_light.IsLit())
        {
            Extinguish();
        }
    }

    private void LightUp()
    {
        _lightLinker.TurnOn();
        _light.LightUp();
        _lightSensor.gameObject.SetActive(false);
        
        _animator.SetTrigger("turnOn");
        _audio.clip = _turnOnSound;
        _audio.loop = false;
        _audio.Play();
    } 
 
    public void Extinguish() 
    {
        _lightSensor.gameObject.SetActive(true);
        _lightLinker.TurnOff();
        _light.Extinguish();
        _audio.clip = _turnOffSound;
        _audio.loop = false;
        _audio.Play();
    }
    public void Victory()
    {
        _light.Victory();
        _animator.SetTrigger("victory");
    }

}
