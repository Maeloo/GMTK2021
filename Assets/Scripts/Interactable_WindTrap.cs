using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable_WindTrap : Interactable
{

    public ParticleSystem particles;
    public BoxCollider collision;

    private AudioSource _audio;
    
    // Start is called before the first frame update
    void Awake()
    {
        _audio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    protected override void OnActivation()
    {
        particles.Play();
        collision.enabled = true;
        _audio.Play();
    }

    protected override void OnDeactivation()
    {
        particles.Stop();
        collision.enabled = false;
        _audio.Stop();
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.gameObject.tag);
    }
}
