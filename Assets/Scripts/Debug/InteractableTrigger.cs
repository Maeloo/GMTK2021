using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableTrigger : MonoBehaviour
{
    [SerializeField] private Interactable _interactable;

    private List<Collider> _colliders;

    private void Start()
    {
        _colliders = new List<Collider>();
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (!other.GetComponent<Unit>()) return;
        if (_colliders.Contains(other)) return;
     
        _colliders.Add(other);
        
        UpdateInteraction();
    }
    
    private void OnTriggerExit(Collider other)
    {
        if (!_colliders.Contains(other)) return;
        
        _colliders.Remove(other);

        UpdateInteraction();
    }

    private void UpdateInteraction()
    {
        var bShouldActivate = _colliders.Count > 0;

        if (_interactable.IsActive() && !bShouldActivate)
        {
            _interactable.ToggleInteraction();
        }
        
        if (_interactable.IsInactive() && bShouldActivate)
        {
            _interactable.ToggleInteraction();
        }
    }
    
}
