using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuController : MonoBehaviour
{
    public string _FirstLevel;
    public string _MainMenu;
    public string _Controls;
    public string _Credits;

    public void GoToFirstLevel()
    {
        MoveToScene(_FirstLevel);
    }
    
    public void GoToMainMenu()
    {
        MoveToScene(_MainMenu);
    }
    
    public void GoToControls()
    {
        MoveToScene(_Controls);
    }

    public void GoToCredits()
    {
        MoveToScene(_Credits);
    }

    public void QuitGame()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit ();
#endif
    }
    
    private void MoveToScene(string Name)
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(Name);
    }
}
