using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Serialization;

public class LightEmitterController : MonoBehaviour
{
    private bool debug = true;

    private bool isLit = true;

    public int baseLightLevel = 1;

    public LightLevelSettings LightLevelSettings;
    private LightLevelSettings.LightLevel currentLightLevel;
    private Animator _animator;

    private float TargetLightLevel = 0.0f;

    public Light lightSource;
    public CapsuleCollider lightCollider;
    public TextMesh debugText;
    public GameObject lightDecal;

    // Start is called before the first frame update
    void Awake()
    {
        _animator = GetComponentInParent<Animator>();
    }

    private void OnValidate()
    {
        UpdateLightLevel(baseLightLevel);
    }

    public bool IsLit()
    {
        return isLit;
    }

    public void LightUp()
    {
        isLit = true;

        if (_animator.runtimeAnimatorController != null)
        {
            _animator.SetBool("isTorchOn", true);
        }

        SetLightLevel(baseLightLevel);
    }

    public void Extinguish()
    {
        SetLightLevel(0);
        isLit = false;

        if (_animator.runtimeAnimatorController != null)
        {
            _animator.SetBool("isTorchOn", false);
            _animator.SetTrigger("turnOff");
        }
    }

    public void Victory()
    {
        SetLightLevel(Mathf.CeilToInt(TargetLightLevel + 1.0f));
    }

// Update is called once per frame
    void Update()
    {
        if (Math.Abs(currentLightLevel.Level - TargetLightLevel) > 0.00001)
        {
            float newLevel = InterpTo(currentLightLevel.Level, TargetLightLevel, LightLevelSettings.InterpolationSpeed, Time.deltaTime);
            UpdateLightLevel(newLevel);
        }
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = currentLightLevel.Color;
        Gizmos.DrawSphere(transform.position, currentLightLevel.Radius);
    }

    public void SetLightLevel(int newLevel)
    {
        if (IsLit())
        {
            //Debug.Log("NewLightLevel " + newLevel);
            TargetLightLevel = newLevel;    
        }
    }

    void UpdateLightLevel(float level)
    {
        if (IsLit())
        {
            //Debug.Log("UpdateLightLevel " + level);    
        }
        ApplyLightLevel(LightLevelSettings.GetLightLevel(level));
    }
    
    void ApplyLightLevel(LightLevelSettings.LightLevel val)
    {
        currentLightLevel = val;
        //Debug.Log("ApplyLightLevel " + val.Level);

        if (lightSource)
        {
            lightSource.range = val.Radius;
            lightSource.intensity = val.LightIntensity;
            lightSource.color = val.Color;    
        }

        if (lightCollider)
        {
            if (val.Radius < 0.001f)
            {
                lightCollider.enabled = false;
            }
            else
            {
                lightCollider.enabled = true;
                lightCollider.radius = val.Radius;
                lightCollider.height = val.Radius * 2.5f;    
            }
        }

        if (lightDecal)
        {
            lightDecal.transform.localScale = new Vector3(val.Radius, val.Radius, val.Radius);    
        }

        if (debug)
        {
            if (debugText)
            {
                debugText.text = currentLightLevel.Level.ToString("F1");    
            }    
        }
        
    }
    
    float InterpTo(float Current, float Target, float DeltaTime, float InterpSpeed)
    {
        // If no interp speed, jump to target value
        if( InterpSpeed <= 0.0f )
        {
            return Target;
        }

        // Distance to reach
        float Dist = Target - Current;

        if (Mathf.Abs(Dist) < 0.05)
        {
            return Target;
        }
        
        float DeltaMove = Dist * Mathf.Clamp( DeltaTime * InterpSpeed, 0.0f, 1.0f);
        //Debug.Log("InterpTo " + " Dist " + Dist + " fac " + Mathf.Clamp( DeltaTime * InterpSpeed, 0.0f, 1.0f) + " Move " + DeltaMove);
        return Current + DeltaMove;
    }
}
