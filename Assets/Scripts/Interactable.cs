using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum InteractionState
{
    Inactive,
    Active
}

public class Interactable : MonoBehaviour
{
    
    #region  Virtual Functions
    protected virtual bool IsInteractable_Internal() { return true; }
    protected virtual void OnInteractInteraction() { }
    protected virtual void OnActivation() { }
    protected virtual void OnDeactivation() { }
    #endregion

    private InteractionState _state = InteractionState.Inactive;

    void Start()
    {
        switch (_state)
        {
            case InteractionState.Active:
                Activate();
                return;
                
            case InteractionState.Inactive:
                Deactivate();
                return;
        }
    }

    public bool IsActive()
    {
        return _state == InteractionState.Active;
    }
    
    public bool IsInactive()
    {
        return _state == InteractionState.Inactive;
    }
    
    public void ToggleInteraction()
    {
        if (IsInteractable())
        {
            OnInteractInteraction();

            switch (_state)
            {
                case InteractionState.Active:
                    Deactivate();
                    return;
                
                case InteractionState.Inactive:
                    Activate();
                    return;
            }
        }
    }
    
    private bool IsInteractable()
    {
        return IsInteractable_Internal();
    }

    public void Activate()
    {
        OnActivation();
        _state = InteractionState.Active;
    }
    
    public void Deactivate()
    {
        OnDeactivation();
        _state = InteractionState.Inactive;
    }
}
