using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class FirePit : MonoBehaviour
{
    [SerializeField] private GameObject _prefabIcon;
    
    public bool _startLit = false;
    
    public LightEmitterController _light;
    public LightLinker _lightLinker;

    private FirePitIcon _icon;
    private List<Unit> _units; // nearby units
    private AudioSource _audio;

    // Start is called before the first frame update
    void Start()
    {
        var iconInstance = Instantiate(_prefabIcon);
        iconInstance.transform.SetParent(FindObjectOfType<Canvas>().transform);
        
        _icon = iconInstance.GetComponent<FirePitIcon>();
        _icon.Register(this);

        _units = new List<Unit>();

        _audio = GetComponent<AudioSource>();

        if (_startLit)
        {
            LightUp();
        }
        else
        {
            Extinguish();
        }
    }

    private bool IsLit()
    {
        return _light.IsLit();
    }

    public void ToggleFire()
    {
        if (IsLit())
        {
            Extinguish();
            _audio.Stop();
        }
        else
        {
            LightUp();
            _audio.Play();
        }
    }
    
    public void LightUp() 
    {
        _lightLinker.TurnOn();
        _light.LightUp();

       

    } 
 
    public void Extinguish() 
    {
        _lightLinker.TurnOff();
        _light.Extinguish();

        
    }
    
    private void OnTriggerEnter(Collider other)
    {
        var unit = other.GetComponent<Unit>();
        if (unit != null)
        {
            unit.NotifyNearFirePit(this);

            if (!_units.Contains(unit))
            {
                _units.Add(unit);
            }
        }
    }
    
    private void OnTriggerExit(Collider other)
    {
        var unit = other.GetComponent<Unit>();
        if (unit != null)
        {
            unit.NotifyLeaveFirePit();
            
            if (_units.Contains(unit))
            {
                _units.Remove(unit);
            }
        }
    }

    protected void Update()
    {
        UpdateIcon();
    }

    private void UpdateIcon()
    {
        var bHasSelectUnit = false;
        foreach (var unit in _units)
        {
            if (!unit.IsSelected()) continue;
            bHasSelectUnit = true;
            break;
        }
        
        if (_units.Count > 0 && !_icon.gameObject.activeSelf && bHasSelectUnit)
        {
            _icon.gameObject.SetActive(true);
        }
        else if (!bHasSelectUnit || (_units.Count == 0 && _icon.gameObject.activeSelf))
        {
            _icon.gameObject.SetActive(false);
        }
        
        if (IsLit())
        {
            _icon.SetIconOff();
        }
        else
        {
            _icon.SetIconOn();
        }
    }
    
}
