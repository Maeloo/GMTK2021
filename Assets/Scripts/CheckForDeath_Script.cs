using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckForDeath_Script : MonoBehaviour
{
    private Unit[] _units;

    public IngameMenuController menu;

    // Start is called before the first frame update
    void Start()
    {
        _units = Object.FindObjectsOfType<Unit>();
        if (_units.Length <= 0)
        {
            Debug.LogWarning("Did not find any units in the scene.");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (_units.Length > 0) // only check if we have any units
        {
            foreach (var unit in _units)
            {
                if (unit.IsInLight())
                {
                    return;
                }
            }
            
            //did not find any units left in the light
            YouLoose();
        }

    }

    private void YouLoose()
    {
        Debug.Log("A looser is you.");
        gameObject.SetActive(false);
        menu.OpenLooseScreen();
    }
}
