using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{

    [SerializeField] private float _rayCastDistance = 100f;
    [SerializeField] private float _maxLightTorchDistance = 4f;
    [SerializeField] private RectTransform _selectionRectangle;
    [SerializeField] private KeyCode _lightOnInput;
    [SerializeField] private KeyCode _lightOffInput;
    [SerializeField] private KeyCode _toggleBonfireInput;
    [SerializeField] private GameObject _iconTorchOn;
    [SerializeField] private GameObject _iconTorchOff;
    [SerializeField] private List<Color> _colorPickerPool;
    [SerializeField] private GameObject _arrowPrefab;

    private List<Color> _colorPool;
    private List<Unit> _selectedUnits;
    private Vector2 _lastClickPosition;
    private Camera _camera;
    private Arrow _arrowInstance;
    private IEnumerable<Unit> _units;

    private void Start()
    {
        Reset();
    }

    public void Reset()
    {
        _selectedUnits = new List<Unit>();
        _camera = GetComponent<Camera>();
        _units = FindObjectsOfType<Unit>();
        _arrowInstance = Instantiate(_arrowPrefab).GetComponent<Arrow>();
        _colorPool = _colorPickerPool;
    }

    public Color PopColor()
    {
        var rand = Random.Range(0, _colorPool.Count - 1);
        Color color = _colorPool[rand];
        _colorPool.RemoveAt(rand);
        return color;
    }
    
    /// Listen to mouse inputs
    private void HandleMouseInputs()
    {
        // ------------------------------------------
        // Left Click
        
        // PRESSED
        if (Input.GetMouseButtonDown(0))
        {
            _lastClickPosition = Input.mousePosition;
            
        }
        
        // HOLD
        if (Input.GetMouseButton(0))
        {
            if (Vector3.Distance(_lastClickPosition, Input.mousePosition) > 2f)
            {
                _selectionRectangle.gameObject.SetActive(true);

                var lb = new Vector2
                {
                    x = Mathf.Min(_lastClickPosition.x, Input.mousePosition.x),
                    y = Mathf.Min(_lastClickPosition.y, Input.mousePosition.y)
                };
                _selectionRectangle.offsetMin = lb;

                var tr = new Vector2
                {
                    x = Mathf.Max(_lastClickPosition.x, Input.mousePosition.x),
                    y = Mathf.Max(_lastClickPosition.y, Input.mousePosition.y)
                };
                _selectionRectangle.offsetMax = tr;
            }
        }

        // RELEASED
        if (Input.GetMouseButtonUp(0))
        {
            _selectionRectangle.gameObject.SetActive(false);
            
            foreach (var unit in _selectedUnits)
            {
                unit.Deselect();
            }
            _selectedUnits.Clear();

            if (Vector3.Distance(_lastClickPosition,Input.mousePosition) <= 2f)
            {
                System.Diagnostics.Debug.Assert(_camera != null, nameof(_camera) + " != null");
                var ray = _camera.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out var hit, _rayCastDistance))
                {
                    var hitUnit = hit.collider.GetComponent<Unit>();
                    if (hitUnit != null)
                    {
                        _selectedUnits.Add(hitUnit);
                        hitUnit.Select(this);
                    }
                }
            }
            else
            {
                _selectedUnits = GetUnitsInRectangle();
            }
        }

        // ------------------------------------------
        // Right Click
        
        if (Input.GetMouseButtonDown(1))
        {
            if (_selectedUnits.Count == 0) return;
            
            System.Diagnostics.Debug.Assert(_camera != null, nameof(_camera) + " != null");
            var ray = _camera.ScreenPointToRay(Input.mousePosition);
            
            if (!Physics.Raycast(ray, out var hit, _rayCastDistance)) return;
            if (!hit.collider.CompareTag("Ground")) return;
            
            foreach (var unit in _selectedUnits)
            {
                unit.MoveTo(hit.point);
            }
            
            _arrowInstance.SpawnAtPosition(hit.point);
        }
        
        // ------------------------------------------
        // Torch Action

        if (_selectedUnits.Count == 1)
        {
            if (Input.GetKeyDown(_lightOnInput))
            {
                _selectedUnits[0].OnTorchOnInput();
            }
        
            if (Input.GetKeyDown(_lightOffInput))
            {
                _selectedUnits[0].OnTorchOffInput();
            }
        }

        if (Input.GetKeyDown(_toggleBonfireInput))
        {
            if (_selectedUnits.Count > 0)
            {
                var ignoreFirePits = new List<FirePit>();
                
                foreach (var selectedUnit in _selectedUnits)
                {
                    if (!selectedUnit.HasNearFirePit(out var firePit)) continue; // No close by fire pit
                    if (firePit == null || ignoreFirePits.Contains(firePit)) continue; // FirePit not valid
                    if (!firePit._light.IsLit() && (firePit._light.IsLit() || !selectedUnit._light.IsLit())) continue; // Toggle condition not matched
                    
                    firePit.ToggleFire();
                    selectedUnit._animator.SetTrigger("turnOn");
                    ignoreFirePits.Add(firePit);
                }
            }
        }
    }
    
    private List<Unit> GetUnitsInRectangle()
    {
        var rect = new Rect(
            Mathf.Min(_lastClickPosition.x, Input.mousePosition.x),
            Mathf.Min(_lastClickPosition.y, Input.mousePosition.y),
            Mathf.Abs(Input.mousePosition.x - _lastClickPosition.x), 
            Mathf.Abs(Input.mousePosition.y - _lastClickPosition.y));

        var selection = new List<Unit>();
        foreach (var unit in _units)
        {
            var p = _camera.WorldToScreenPoint(unit.transform.position);
            if (!rect.Contains(p)) continue;
            selection.Add(unit);
            unit.Select(this);
        }

        foreach(var unit in _selectedUnits)
        {
            if (!selection.Contains(unit))
            {
                unit.Deselect();
            }
        }

        return selection;
    }

    private void UpdateHUD()
    {
        // very ugly code sorry tired
        if (_selectedUnits.Count == 1)
        {
            if (_selectedUnits[0]._light.IsLit())
            {
                _iconTorchOn.gameObject.SetActive(false);
                _iconTorchOff.gameObject.SetActive(true);
            }
            else if (_selectedUnits[0].CanTorchUpLight())
            {
                _iconTorchOn.gameObject.SetActive(true);
                _iconTorchOff.gameObject.SetActive(false);  
            }
            else
            {
                _iconTorchOn.gameObject.SetActive(false);
                _iconTorchOff.gameObject.SetActive(false);
            }
        }
        else
        {
            _iconTorchOn.gameObject.SetActive(false);
            _iconTorchOff.gameObject.SetActive(false);
        }
    }

    private void Update()
    {
        HandleMouseInputs();
        UpdateHUD();
    }

    public bool IsUnitNearOtherLitUnit(Unit inUnit)
    {
        foreach (var unit in _units)
        {
            if (unit && unit._light.IsLit())
            {
                return Vector3.Distance(inUnit.transform.position, unit.transform.position) <= _maxLightTorchDistance;
            }
        }
        return false;
    }
}
