using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class CameraController : MonoBehaviour
{
    
    [SerializeField] private float _sideScreenThreshold = 0.05f;
    [SerializeField] private float _maxSpeed = 0.1f;
    [SerializeField] private float _acceleration = 0.05f;
    
    private Collider _constraintVolume;
    private float _speed;

    /// Start is called before the first frame update
    private void Start()
    {
        _speed = 0f;

        var colliders = FindObjectsOfType<Collider>();
        foreach (var colliderElem in colliders)
        {
            if (colliderElem.CompareTag("CameraConstraints"))
            {
                _constraintVolume = colliderElem;
            }
        }
    }

    /// Update is called once per frame
    private void Update()
    {
        var mousePos = Input.mousePosition;
        {
            var right2D = Vector3.ProjectOnPlane(transform.right, Vector3.up).normalized;
            var forward2D = Vector3.ProjectOnPlane(transform.forward, Vector3.up).normalized;

            //_speed = 0f;

            if (mousePos.x / Screen.width < _sideScreenThreshold || Input.GetKey(KeyCode.LeftArrow))
            {
                _speed = Mathf.Min(_speed + _acceleration * Time.deltaTime, _maxSpeed);
                transform.position -= right2D * _speed;
            }
            
            if (mousePos.x / Screen.width > 1f - _sideScreenThreshold || Input.GetKey(KeyCode.RightArrow))
            {
                _speed = Mathf.Min(_speed + _acceleration * Time.deltaTime, _maxSpeed);
                transform.position += right2D * _speed;
            }
            
            if (mousePos.y / Screen.height < _sideScreenThreshold || Input.GetKey(KeyCode.DownArrow))
            {
                _speed = Mathf.Min(_speed + _acceleration * Time.deltaTime, _maxSpeed);
                transform.position -= forward2D * _speed;
            }
            
            if (mousePos.y / Screen.height > 1f - _sideScreenThreshold || Input.GetKey(KeyCode.UpArrow))
            {
                _speed = Mathf.Min(_speed + _acceleration * Time.deltaTime, _maxSpeed);
                transform.position += forward2D * _speed;
            }    

            // Clamping position to stay within the constraint volume
            var checkPosition = transform.position;
            checkPosition.y = _constraintVolume.transform.position.y;
            if (!_constraintVolume.bounds.Contains(checkPosition))
            {
                var clampedPosition = _constraintVolume.bounds.ClosestPoint(checkPosition);
                clampedPosition.y = transform.position.y;
                transform.position = clampedPosition;
            }
        }
    }
}
