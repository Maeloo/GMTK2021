using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class MovingPlatform : Interactable
{
    [SerializeField] private Transform _defaultPosition;
    [SerializeField] private Transform _activePosition;
    [SerializeField] private float _speed = 0.1f;
    [SerializeField] private GameObject _defaultBlockingWall;
    [SerializeField] private GameObject _activeBlockingWall;

    private float _time = 0f;

    private List<Unit> _units;

    protected void Start()
    {
        _units = new List<Unit>();
    }

    public void Register(Unit unit)
    {
        if (!_units.Contains(unit))
        {
            _units.Add(unit);
            unit.transform.SetParent(transform); 
        }
    }
    
    public void Unregister(Unit unit)
    {
        if (_units.Contains(unit))
        {
            _units.Remove(unit);
            unit.transform.SetParent(null); 
        }
    }

    protected void Update()
    {
        // Update movement blend alpha
        _time = Mathf.Clamp01(_time + (Time.deltaTime * _speed * (IsActive() ? 1f : -1f)));
        
        // Update movement
        var prevPosition = transform.position;
        transform.position = Vector3.Lerp(_defaultPosition.position, _activePosition.position, _time);
        var deltaPosition = transform.position - prevPosition;
        
        // Update units movement
        foreach (var unit in _units)
        {
            unit.AddMoveOffset(deltaPosition);
        }
        
        UpdateWallsCollision();
    }

    private void UpdateWallsCollision()
    {
        // Inactived
        if (_time <= 0f)
        {
            _activeBlockingWall.SetActive(true);
            _defaultBlockingWall.SetActive(false);
        }
        
        // Activated
        else if (_time >= 1f)
        {
            _defaultBlockingWall.SetActive(true);
            _activeBlockingWall.SetActive(false);
        }

        // Moving
        else
        {
            _defaultBlockingWall.SetActive(true);
            _activeBlockingWall.SetActive(true);
        }
    }
    
    void OnDrawGizmosSelected()
    {
        var extent = GetComponent<BoxCollider>().bounds.extents;
        
        Gizmos.color = Color.yellow;
        Gizmos.DrawCube(_defaultPosition.position, extent);
        
        Gizmos.color = Color.green;
        Gizmos.DrawCube(_activePosition.position, extent);
        
        Gizmos.color = Color.red;
        Gizmos.DrawLine(_activePosition.position, _defaultPosition.position);
    }
}
