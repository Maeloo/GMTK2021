using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageTrigger : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Character"))
        {
            Unit unit = other.gameObject.GetComponent<Unit>();
            Debug.Assert(unit);
            unit.Extinguish();
        }
    }
}
