using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightLinker : MonoBehaviour
{
    public LightEmitterController _light;
    
    public int accumulatedLightLevel = 0;
    
    public List<LightLinker> LinkedLightLinker;
    
    // Start is called before the first frame update
    void Start()
    {
        accumulatedLightLevel = _light.baseLightLevel;
    }

    public void TurnOn()
    {
        gameObject.SetActive(true);
        foreach (var Lighter in LinkedLightLinker)
        {
            Lighter.AddLinkedObject(this);
        }
    }

    public void TurnOff()
    {
        gameObject.SetActive(false);
        foreach (var Lighter in LinkedLightLinker)
        {
            Lighter.RemoveLinkedObject(this);
        }
    }
    
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.cyan;
        foreach (var obj in LinkedLightLinker)
        {
            Gizmos.DrawLine(transform.position, obj.transform.position);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            LightLinker otherLightLinker = other.gameObject.GetComponent<LightLinker>();
            AddLinkedObject(otherLightLinker);
        }
    }

    public void AddLinkedObject(LightLinker Link)
    {
        if (Link._light.IsLit() && !LinkedLightLinker.Contains(Link))
        {
            LinkedLightLinker.Add(Link);
            accumulatedLightLevel += Link._light.baseLightLevel;

            _light.SetLightLevel(accumulatedLightLevel);
        }
    }
    
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            LightLinker otherLightLinker = other.gameObject.GetComponent<LightLinker>();
            RemoveLinkedObject(otherLightLinker); 
        }
    }
    
    public void RemoveLinkedObject(LightLinker Link)
    {
        if (LinkedLightLinker.Contains(Link))
        {
            LinkedLightLinker.Remove(Link);
            accumulatedLightLevel -= Link._light.baseLightLevel;

            _light.SetLightLevel(accumulatedLightLevel);
        }
    }
}
