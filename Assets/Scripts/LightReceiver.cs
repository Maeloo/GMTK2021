using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightReceiver : MonoBehaviour
{
    public LightSensor lightSensor;

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("OnTriggerEnter " + other.gameObject.ToString() + " " + other.gameObject.tag);

        if (other.gameObject.CompareTag("Light"))
        {
            LightEmitterController lightEmitterController = other.gameObject.GetComponent<LightEmitterController>();
            Debug.Assert(lightEmitterController);
            if (lightEmitterController)
            {
                lightSensor.OnAddLightInRange(lightEmitterController);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        //Debug.Log("OnTriggerExit " + other.gameObject.ToString() + " " + other.gameObject.tag);
        
        if (other.gameObject.CompareTag("Light"))
        {
            LightEmitterController lightEmitterController = other.gameObject.GetComponent<LightEmitterController>();
            Debug.Assert(lightEmitterController);
            if (lightEmitterController)
            {
                lightSensor.OnRemoveLightInRange(lightEmitterController);
            }
        }
    }
}