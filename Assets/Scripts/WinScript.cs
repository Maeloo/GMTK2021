using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinScript : Interactable
{
    public IngameMenuController menu;
    
    protected override void OnActivation()
    {
        Debug.Log("A winner is you!");
        Unit[] units = Object.FindObjectsOfType<Unit>();
        foreach (var unit in units)
        {
            unit.Victory();
        }
        
        StartCoroutine(WinScreenDelay());
    }

    IEnumerator WinScreenDelay()
    {
        yield return new WaitForSeconds(3);
        gameObject.SetActive(false);
        menu.OpenWinScreen();
    }
}
