using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class IngameMenuController : MonoBehaviour
{
    public static bool IsPaused = false;

    public GameObject PauseMenu;
    public GameObject WinScreen;
    public GameObject LooseScreen;
    public GameObject ControllScreen;

    public SceneController SceneController;

    private void Start()
    {
        Resume();
        PauseMenu.SetActive(false);
        WinScreen.SetActive(false);
        LooseScreen.SetActive(false);
        ControllScreen.SetActive(false);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            if (IsPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }

    public void Restart()
    {
        Resume();
        SceneController.Restart();
    }
    
    public void NextLevel()
    {
        SceneController.ToNextLevel();
    }

    public void Quit()
    {
        SceneController.ToMainMenu();
    }
    
    public void Resume()
    {
        PauseMenu.SetActive(false);
        CloseControls();
        Time.timeScale = 1.0f;
        IsPaused = false;
    }

    void Pause()
    {
        PauseMenu.SetActive(true);
        Time.timeScale = 0.0f;
        IsPaused = true;
    }

    public void OpenControls()
    {
        ControllScreen.SetActive(true);
    }
    
    public void CloseControls()
    {
        ControllScreen.SetActive(false);
    }
    
    public void OpenWinScreen()
    {
        WinScreen.SetActive(true);
    }
    
    public void CloseWinScreen()
    {
        WinScreen.SetActive(false);
    }
    
    public void OpenLooseScreen()
    {
        LooseScreen.SetActive(true);
    }
    
    public void CloseLooseScreen()
    {
        LooseScreen.SetActive(false);
    }
}
