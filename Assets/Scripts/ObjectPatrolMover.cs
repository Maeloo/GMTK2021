using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using Debug = UnityEngine.Debug;

public class ObjectPatrolMover : MonoBehaviour
{
    public GameObject Point1;
    public GameObject Point2;

    public float Speed = 1;

    private bool Forward = true;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 SelfLocation = transform.position;
        Vector3 TargetLocation = Forward ? Point1.transform.position : Point2.transform.position;

        Vector3 Dist = (TargetLocation - SelfLocation);
        float Distance = Dist.magnitude;
        if (Distance <= 0.05)
        {
            SwitchDirection();
            return;
        }
        
        Vector3 Dir = Dist.normalized;
        Vector3 Move = Dir * (Speed * Time.deltaTime);

        transform.Translate(Move);
    }

    private void SwitchDirection()
    {
        Forward = !Forward;
    }
}
