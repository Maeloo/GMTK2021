using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour
{
    [SerializeField] private float _lerpTime = 0.5f;
    
    private float _elapsedTime;
    private Vector3 _targetPosition;

    protected  void Start()
    {
        gameObject.SetActive(false);
    }
    
    public void SpawnAtPosition(Vector3 position)
    {
        transform.position = position + Vector3.up * 3f;
        
        _targetPosition = position + Vector3.up * 2f;
        _elapsedTime = 0f;
        
        gameObject.SetActive(true);
    }
    
    protected void Update()
    {
        _elapsedTime += Time.deltaTime;
        var t = Mathf.Clamp01(_elapsedTime / _lerpTime);
        transform.position = Vector3.Slerp(transform.position, _targetPosition, t);
        transform.RotateAround(transform.position, transform.up, 1f);

        if (t >= 1f)
        {
            gameObject.SetActive(false);
        }
    }
}
