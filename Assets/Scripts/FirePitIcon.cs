using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FirePitIcon : MonoBehaviour
{

    [SerializeField] private Image Circle;
    [SerializeField] private Image Nope;
    
    private FirePit _firePit;
    private RectTransform _rectTransform;

    public void Register(FirePit firePit)
    {
        _firePit = firePit;
        _rectTransform = GetComponent<RectTransform>();
        gameObject.SetActive(false);
    }

    public void SetIconOn()
    {
        Circle.color = Color.white;
        Nope.gameObject.SetActive(false);
    }
    
    public void SetIconOff()
    {
        Circle.color = Color.red;
        Nope.gameObject.SetActive(true);
    }
    
    protected void Update()
    {
        var location = Camera.main.WorldToScreenPoint(_firePit.transform.position + Vector3.up * 3.5f);
        location.y += Mathf.Sin(Time.time * 6f) * 6f;
        _rectTransform.position = location;
    }
}
