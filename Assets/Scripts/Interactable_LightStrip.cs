using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable_LightStrip : Interactable
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected override void OnActivation()
    {
        gameObject.SetActive(true);
    }

    protected override void OnDeactivation()
    {
        gameObject.SetActive(false);
    }
}
