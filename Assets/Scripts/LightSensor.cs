using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;
using UnityEngine.UI;

public class LightSensor : MonoBehaviour
{
    public TextMesh text;
    
    private List<LightEmitterController> Lights = new List<LightEmitterController>();

    public List<Interactable> TurnOnWhenLit = new List<Interactable>();
    public List<Interactable> TurnOnWhenDark = new List<Interactable>();

    private bool IsOn = false;
    
    // Start is called before the first frame update
    void Start()
    {
        TurnDark();
    }
    
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.cyan;
        foreach (var obj in Lights)
        {
            Gizmos.DrawLine(transform.position, obj.transform.position);
        }
    }

    // Update is called once per frame
    void Update()
    {
        CheckStatus();
    }

    void CheckStatus()
    {
        if (IsOn)
        {
            if (Lights.Count > 0)
            {
                foreach (var light in Lights)
                {
                    if (LineCheck(light)) // check all, return if any line is free
                    {
                        return;
                    }
                }
            }
            
            TurnDark();
        }
        else
        {
            if (Lights.Count > 0)
            {
                foreach (var light in Lights)
                {
                    if (LineCheck(light))
                    {
                        TurnLit();
                        return;
                    }
                }
            }
        }
    }

    bool LineCheck(LightEmitterController OtherLight) //return true if line is clear
    {
        return true; // skip line checks for now
        
        /*Vector3 start = transform.position;
        Vector3 end = OtherLight.transform.position;
        
        Debug.DrawLine(start, end, Color.red);

        RaycastHit[] hits;
        hits = Physics.RaycastAll(start, end, Mathf.Infinity);
        
        // Does the ray intersect any objects excluding the player layer
        if (hits.Length > 0)
        {
            return false;
        }
        else
        {
            return true;
        }*/
    }

    public bool IsLit()
    {
        return IsOn;
    }

    private void TurnLit()
    {
        IsOn = true;

        if (text)
        {
            text.text = "Lit";
        }

        foreach (var obj in TurnOnWhenDark)
        {
            obj.Deactivate();
        }
        foreach (var obj in TurnOnWhenLit)
        {
            obj.Activate();
        }
    }

    private void TurnDark()
    {
        IsOn = false;

        if (text)
        {
            text.text = "Dark";    
        }
        
        
        foreach (var obj in TurnOnWhenDark)
        {
            obj.Activate();
        }
        foreach (var obj in TurnOnWhenLit)
        {
            obj.Deactivate();
        }
    }

    public void OnAddLightInRange(LightEmitterController Light)
    {
        //Debug.Log(this.ToString() + " OnAddLightInRange " + Light.ToString());
        Lights.Add(Light);
    }
    
    public void OnRemoveLightInRange(LightEmitterController Light)
    {
        //Debug.Log(this.ToString() + " OnRemoveLightInRange " + Light.ToString());
        Lights.Remove(Light);
    }
}
